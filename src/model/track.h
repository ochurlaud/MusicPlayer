#ifndef TRACK_H
#define TRACK_H

#include <QString>
#include <QUrl>

#include "musicplayer_model_export.h"
#include "entity.h"
#include "album.h"
#include "artist.h"

class MUSICPLAYER_MODEL_EXPORT Track : public Entity
{
    /*
    Q_OBJECT
    Q_PROPERTY(QString name READ name)
    Q_PROPERTY(QString sortName READ sortName)
    Q_PROPERTY(int trackNumber READ trackNumber)
    Q_PROPERTY(Artist trackArtist READ trackArtist)
    Q_PROPERTY(Artist composer READ composer)
    Q_PROPERTY(Artist performer READ performer)
    Q_PROPERTY(Artist conductor READ conductor)
    Q_PROPERTY(Album album READ album)
*/
public:
    Track();

    /**
     * @brief Get sortName
     */
    QString sortName() const { return m_sortName; }

    /**
     * @brief Set sortName
     */
    void setSortName(const QString& sortName) { m_sortName = sortName; }

    /**
     * @brief Get trackNumber
     */
    int trackNumber() const { return m_trackNumber; }

    /**
     * @brief Set trackNumber
     */
    void setTrackNumber(const int trackNumber) { m_trackNumber = trackNumber; }

    /**
     * @brief Get trackArtist
     */
    Artist trackArtist() const { return m_trackArtist; }

    /**
     * @brief Set trackArtist
     */
    void setTrackArtist(const Artist& trackArtist) { m_trackArtist = trackArtist; m_trackArtist.setType(ArtistType::TrackArtist);  }

    /**
     * @brief Get composer
     */
    Artist composer() const { return m_composer; }

    /**
     * @brief Set composer
     */
    void setComposer(const Artist& composer) { m_composer = composer; m_composer.setType(ArtistType::Composer); }

    /**
     * @brief Get conductor
     */
    Artist conductor() const { return m_conductor; }

    /**
     * @brief Set conductor
     */
    void setConductor(const Artist& conductor) { m_conductor = conductor; m_conductor.setType(ArtistType::Conductor); }

    /**
     * @brief Get performer
     */
    Artist performer() const { return m_performer; }

    /**
     * @brief Set performer
     */
    void setPerformer(const Artist& performer) { m_performer = performer; m_performer.setType(ArtistType::Performer); }

    /**
     * @brief Get album
     */
    Album album() const { return m_album; }

    /**
     * @brief Set album
     */
    void setAlbum(const Album& album) { m_album = album; }

    /**
     * @brief Get encodedBy
     */
    QString encodedBy() const { return m_encodedBy; }

    /**
     * @brief Set encodedBy
     */
    void setEncodedBy(const QString& encodedBy) { m_encodedBy = encodedBy; }

    /**
     * @brief Get playcount
     */
    int playcount() const { return m_playcount; }

    /**
     * @brief Set playcount
     */
    void setPlaycount(const int playcount) { m_playcount = playcount; }

    /**
     * @brief Get comment
     */
    QString comment() const { return m_comment; }

    /**
     * @brief Set comment
     */
    void setComment(const QString& comment) { m_comment = comment; }

    /**
     * @brief Get userRate
     */
    int userRate() const { return m_userRate; }

    /**
     * @brief Set userRate
     */
    void setUserRate(const int userRate) { m_userRate = userRate; }

    /**
     * @brief Get format
     */
    QString format() const { return m_format; }

    /**
     * @brief Set format
     */
    void setFormat(const QString& format) { m_format = format; }

    /**
     * @brief Get otherTags
     */
    QList<QPair<QString, QString> > otherTags() const { return m_otherTags; }

    /**
     * @brief Set otherTags
     */
    void setOtherTags(const QList<QPair<QString, QString> >& otherTags)  { m_otherTags = otherTags; }

    /**
     * @brief Get duration
     * @return duration in seconds (rounded value)
     */
    int duration() const { return m_duration; }

    /**
     * @brief Set duration
     * @parameter duration in seconds (rouded value)
     */
    void setDuration(const int duration) { m_duration = duration; }

    /**
     * @brief Get BPM
     */
    int BPM() const { return m_BPM; }

    /**
     * @brief Set BPM
     */
    void setBPM(const int bpm) { m_BPM = bpm; }

    /**
     * @brief Get sampleRate
     */
    int sampleRate() const { return m_sampleRate; }

    /**
     * @brief Set sampleRate
     */
    void setSampleRate(int sampleRate) {m_sampleRate = sampleRate; }

    /**
     * @brief Get channelNumber
     */
    int channelNumber() const { return m_channelNumber; }

    /**
     * @brief Set channelNumber
     */
    void setChannelNumber(const int channelNumber) { m_channelNumber = channelNumber; }

    /**
     * @brief Get bitrate
     */
    int bitrate() const { return m_bitrate; }

    /**
     * @brief Set bitrate
     */
    void setBitrate(const int bitrate) { m_bitrate = bitrate; };

    /**
     * @brief Get url
     */
    QUrl url() const { return m_url; }

    /**
     * @brief Set url
     */
    void setUrl(const QUrl& url) { m_url = url; }

    /**
     * @brief Set url from file path
     */
    void setUrlFromFile(const QString& path) { m_url = QUrl::fromLocalFile(path); }

private:
    QString m_sortName; // TITLESORT
    int m_trackNumber; // TRACKNUMBER before '/' (eg: 01/12)
    Artist m_trackArtist; // ARTIST
    Artist m_composer; // COMPOSER
    Artist m_conductor; // CONDUCTOR
    Artist m_performer; // PERFORMER
    Album m_album; // ALBUM

    QString m_encodedBy; // ENCODEDBY
    int m_playcount; // FMPS_PLAYCOUNT ?
    QString m_comment; // COMMENT
    double m_userRate; // See in freedesktop
    QString m_format;

    QList<QPair<QString, QString> > m_otherTags;

    int m_duration; // In seconds, use QTime().addSecs(Track.duration()) to format
    double m_BPM; // BPM
    int m_sampleRate;
    int m_channelNumber;
    int m_bitrate;

    QUrl m_groupPath;
    QUrl m_url;


/*
    TITLE           o track.name
    ALBUM           o track.album.name
    ARTIST          o track.artist.name (type = artist)
    ALBUMARTIST     o track.album.artist.name (type = artist)
    SUBTITLE        x
    TRACKNUMBER     o track.trackNumber/track.album.numberOfTracks
    DISCNUMBER      o track.album.discNumber / track.album.numberofDiscs
    DATE            o track.album.year
    ORIGINALDATE    x
    GENRE           o track.album.genreList
    COMMENT         o track.comment

Sort names:

    TITLESORT       o track.sortName
    ALBUMSORT       o track.album.sortName
    ARTISTSORT      o track.trackartist.sortName
    ALBUMARTISTSORT o track.album.artist.sortName

Credits:

    COMPOSER        o track.composer.name
    LYRICIST
    CONDUCTOR       o track.conductor.name
    REMIXER
    PERFORMER:<XXXX>o track.performer.name

Other tags:

    ISRC
    ASIN
    BPM             o track.BPM
    COPYRIGHT
    ENCODEDBY       o track.encodedBy
    MOOD
    COMMENT
    MEDIA
    LABEL
    CATALOGNUMBER
    BARCODE

MusicBrainz identifiers:

    MUSICBRAINZ_TRACKID
    MUSICBRAINZ_ALBUMID
    MUSICBRAINZ_RELEASEGROUPID
    MUSICBRAINZ_WORKID
    MUSICBRAINZ_ARTISTID
    MUSICBRAINZ_ALBUMARTISTID
    ACOUSTID_ID
    ACOUSTID_FINGERPRINT
    MUSICIP_PUID
*/

};

#endif // TRACK_H
