#ifndef ENTITY_H
#define ENTITY_H

#include <QString>

#include "musicplayer_model_export.h"

class MUSICPLAYER_MODEL_EXPORT Entity
{
public:
    Entity();

    /**
     * @brief Get id
     */
    int id() const { return m_id; }

    /**
     * @brief Set id
     */
    void setId(const int id) { m_id = id; }

    /**
     * @brief Get name
     */
    QString name() const { return m_name; }

    /**
     * @brief Set name
     */
    void setName(const QString& name) { m_name = name; }

protected:
    int m_id;
    QString m_name;
};

#endif // ENTITY_H
