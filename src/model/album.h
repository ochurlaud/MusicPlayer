#ifndef ALBUM_H
#define ALBUM_H

#include <QObject>
#include <QUrl>

#include "musicplayer_model_export.h"
#include "entity.h"
#include "artist.h"
#include "genre.h"

class MUSICPLAYER_MODEL_EXPORT Album : public Entity
{
public:
    explicit Album();

    Album(const Album&);

    /**
     * @brief Get sortName
     */
    QString sortName() const { return  m_sortName; }

    /**
     * @brief Set sortName
     */
    void setSortName(const QString& sortName) { m_sortName = sortName; }

    /**
     * @brief Get genreList
     */
    QList<Genre> genreList() const { return m_genreList; }

    /**
     * @brief Set genreList
     */
    void setGenreList(const QList<Genre> genreList) { m_genreList = genreList; }

    /**
     * @brief Get number of tracks
     */
    int numberOfTracks() const { return m_numberOfTracks; }

    /**
     * @brief Set number of tracks
     */
    void setNumberOfTracks(const int numberOfTracks) { m_numberOfTracks = numberOfTracks; }

    /**
     * @brief Get disc number
     * @note Disc number is <b>not</b> the number of discs.
     */
    int discNumber() const { return m_discNumber; }

    /**
     * @brief Set disc number
     * @note Disc number is <b>not</b> the number of discs.
     */
    void setDiscNumber(const int discNumber) { m_discNumber = discNumber; }

    /**
     * @brief Get number of discs in the album
     */
    int numberOfDiscs() const { return m_numberOfDiscs; }

    /**
     * @brief Set number of discs in the album
     */
    void setNumberOfDiscs(const int numberOfDiscs) { m_numberOfDiscs = numberOfDiscs; }

    /**
     * @brief Get artist
     */
    Artist artist() const { return m_artist; }

    /**
     * @brief Set artist
     */
    void setArtist(const Artist& artist) { m_artist = artist; m_artist.setType(ArtistType::AlbumArtist); }

    /**
     * @brief Get year
     */
    int year() const { return m_year; }

    /**
     * @brief Set year
     */
    void setYear(const int year) { m_year = year; }

    /**
     * @brief Get cover url
     */
    QUrl coverUrl() const { return m_coverUrl; }

    /**
     * @brief Set cover url
     */
    void setCoverUrl(const QUrl& url) { m_coverUrl = url; }

    /**
     * @brief Set cover url from a file path
     */
    void setCoverUrlFromPath(const QString& path) { m_coverUrl = QUrl::fromLocalFile(path); }

private:
    QString m_sortName; // ALBUMSORT
    QList<Genre> m_genreList; // GENRE
    int m_numberOfTracks;
    int m_discNumber;
    int m_numberOfDiscs;
    Artist m_artist;
    int m_year;
    QUrl m_coverUrl;
};

Q_DECLARE_METATYPE(Album)

#endif // ALBUM_H
