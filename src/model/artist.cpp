#include "artist.h"

Artist::Artist()
{

}

Artist::Artist(const Artist& other)
{
    m_id = other.id();
    m_name = other.name();
    m_sortName = other.sortName();
    m_type = other.type();
}
