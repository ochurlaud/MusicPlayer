#include "album.h"

Album::Album()
{

}

Album::Album(const Album& other)
{
    m_id = other.id();
    m_name = other.name();
    m_sortName = other.sortName();
    m_genreList = other.genreList();
    m_numberOfTracks = other.numberOfTracks();
    m_discNumber = other.discNumber();
    m_numberOfDiscs = other.numberOfDiscs();
    m_artist = other.artist();
    m_year = other.year();
    m_coverUrl = other.coverUrl();
}
