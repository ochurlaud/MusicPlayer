#ifndef GENRE_H
#define GENRE_H

#include "musicplayer_model_export.h"
#include "entity.h"

class MUSICPLAYER_MODEL_EXPORT Genre : public Entity
{
public:
    Genre();
};

#endif // GENRE_H
