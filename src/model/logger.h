#ifndef LOGGER_H
#define LOGGER_H

#include <QDebug>
#include <QString>

#include "musicplayer_model_export.h"

#define _ME_ __PRETTY_FUNCTION__

namespace Logger {
    extern int SPACE_SIZE;
}

MUSICPLAYER_MODEL_EXPORT QDebug debugIn(const QString &src="");
MUSICPLAYER_MODEL_EXPORT QDebug debugOut(const QString &src="");
MUSICPLAYER_MODEL_EXPORT QDebug debug(const QString &src="");

#endif // LOGGER_H
