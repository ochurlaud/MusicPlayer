#include "logger.h"

namespace Logger {
    int SPACE_SIZE = 0;

    QString header(const QString& src) {
        if (src.isEmpty()) {
            return QString((SPACE_SIZE)*4, ' ');
        } else  {
            return QString((SPACE_SIZE)*4, ' ') + "\x1b[1m" + '[' + src + ']' + "\x1b[0m";
        }
    }
}

QDebug debugIn(const QString& src)
{

    QString header = Logger::header(src);
    Logger::SPACE_SIZE++;
    return qDebug().noquote() << header;
}

QDebug debugOut(const QString& src)
{
    Logger::SPACE_SIZE--;
    QString header = Logger::header(src);
    return qDebug().noquote() << header;
}

QDebug debug(const QString& src)
{
    QString header = Logger::header(src);
    return qDebug().noquote() << header;
}

