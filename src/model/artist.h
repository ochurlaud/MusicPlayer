#ifndef ARTIST_H
#define ARTIST_H

#include <QObject>

#include "musicplayer_model_export.h"
#include "entity.h"

namespace ArtistType {
enum Type {
        None = 0,
        AlbumArtist = 1,
        TrackArtist = 2,
        Composer = 4,
        Conductor = 8,
        Performer = 16
    };
}

class MUSICPLAYER_MODEL_EXPORT Artist : public Entity
{
public:
    explicit Artist();

    Artist(const Artist&);

    /**
     * @brief Get sort name
     */
    QString sortName() const { return m_sortName; }

    /**
     * @brief Set sort name
     */
    void setSortName(const QString& sortName) { m_sortName = sortName; }

    /**
     * @brief Get type
     */
    int type() const  { return m_type; }

    /**
     * @brief Set type
     * @param type can be...
     */
    void setType(const int type) { m_type |= type; }

private:
    QString m_sortName; // ARTISTSORT or ALBUMARTISTSORT
    int m_type;
};

Q_DECLARE_METATYPE(Artist)

#endif // ARTIST_H
