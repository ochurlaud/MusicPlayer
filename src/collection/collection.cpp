#include "collection.h"

// TagLib
#include <fileref.h>
#include <tag.h>
#include <tpropertymap.h>
#include <mpegfile.h>
#include <id3v2tag.h>

// Qt
#include <QStringList>
#include <QFileInfo>
#include <QDebug>

// MusicPlayer
#include "databasemanager.h"
#include "track.h"

Collection::Collection(QObject* parent) : QObject(parent)
{

}

int Collection::importDirectoryList(QStringList directoryList)
{
    int errorValue = 0;

    if (directoryList.isEmpty())
        return 0;

    Q_FOREACH (QString directoryName, directoryList) {
        QDir directory(directoryName);
        if (directory.exists())
            errorValue |= this->importDirectory(directory);
    }
    return errorValue;
}

int Collection::importDirectory(const QDir& dir)
{
    int errorValue = 0;

    QFileInfoList fileList = dir.entryInfoList(QDir::Files);
    errorValue |= this->importFiles(fileList);

    QFileInfoList subdirList = dir.entryInfoList(QDir::Dirs | QDir::NoDotAndDotDot);
    Q_FOREACH (QFileInfo subdir, subdirList) {
        qDebug() << subdir.absoluteFilePath();
        errorValue |= this->importDirectory( QDir(subdir.absoluteFilePath()) );
    }

    return errorValue;
}

int Collection::importFiles(const QFileInfoList& fileList)
{
    QList<Track> trackList;
    Q_FOREACH (QFileInfo file, fileList) {
        TagLib::File* f;

        QString path = file.absoluteFilePath();
        if (file.suffix() == "mp3") {
            f = new TagLib::MPEG::File(path.toUtf8());
        } else {
            continue; // We didn't implement for the format yet.
        }

        if (f->isValid()) {
            Track track;
            track.setUrlFromFile(path);
            if (f->properties().contains("GENRE")) {
                qDebug() << f->properties()["GENRE"].toString().toCString(true);
            }

            QString albumName;
            QString artistAlbumName;
            QString artistName;
            QString artistSortName;
            QString artistAlbumSortName;
            QString genreName;
            QString title;

            // Tags
            if (f->properties().contains("ALBUM")) {
                albumName = f->properties()["ALBUM"].front().toCString(true);
            }
            if (f->properties().contains("ALBUMARTIST")) {
                artistAlbumName = f->properties()["ALBUMARTIST"].front().toCString(true);
            }
            if (f->properties().contains("GENRE")) {
                genreName = f->properties()["GENRE"].front().toCString(true);
            }
            if (f->properties().contains("ARTIST")) {
                artistName = f->properties()["ARTIST"].front().toCString(true);
            }
            if (f->properties().contains("TITLE")) {
                title = f->properties()["TITLE"].front().toCString(true);
            }

            Artist artistAlbum;
            if (artistAlbumName.isEmpty()) {
                artistAlbum.setName(artistName);
            } else {
                artistAlbum.setName(artistAlbumName);
            }
            Artist artist;
            artistAlbum.setName(artistName);
            Genre genre;
            genre.setName(genreName);
            Album album;
            album.setName(albumName);
            album.setArtist(artistAlbum);
            album.setGenreList(QList<Genre>() << genre);

            track.setAlbum(album);
            track.setTrackArtist(artist);
            track.setName(title);

            // Audio properties
            track.setBitrate(f->audioProperties()->bitrate());
            track.setDuration(f->audioProperties()->lengthInSeconds());
            track.setSampleRate(f->audioProperties()->sampleRate());
            track.setChannelNumber(f->audioProperties()->channels());

            trackList.append(track);
        }
    }
    if (!trackList.isEmpty()) {
        DatabaseManager dm;
        dm.insertTracks(trackList);
    }

return 0;
}
