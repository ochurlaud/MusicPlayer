#ifndef COLLECTION_H
#define COLLECTION_H

#include <QObject>
#include <QDir>

#include "musicplayer_collection_export.h"

class MUSICPLAYER_COLLECTION_EXPORT Collection : public QObject
{
    Q_OBJECT
public:
    explicit Collection(QObject* parent = 0);
    int importDirectoryList(QStringList folderList);
    /* Should move to their own class */
    int importDirectory(const QDir& dir);
    int importFiles(const QFileInfoList& fileList);
signals:

public slots:
};

#endif // COLLECTION_H
