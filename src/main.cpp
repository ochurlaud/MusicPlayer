#include <QGuiApplication>
#include <QQmlApplicationEngine>

int main(int argc, char *argv[])
{
    // Create own Application class
    QGuiApplication app(argc, argv);
    app.setApplicationName("musicplayer");
    app.setApplicationDisplayName("MusicPlayer");
    app.setApplicationVersion("0.0.1");
    app.setOrganizationDomain("kde.org");

    // Move to own Application class
    QQmlApplicationEngine engine;
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));

    return app.exec();
}

