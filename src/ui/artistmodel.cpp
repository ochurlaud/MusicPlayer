#include "artistmodel.h"

#include <QtQml>

#include "databasemanager.h"

ArtistModel::ArtistModel(QObject* parent)
    : QAbstractListModel(parent)
{
    DatabaseManager dm;
    QList<Artist> artistList = dm.getAllArtists(ArtistType::AlbumArtist);
    beginInsertRows(QModelIndex(), m_artistList.count(), m_artistList.count());
    m_artistList << artistList;
    endInsertRows();
}

int ArtistModel::rowCount(const QModelIndex& parent) const
{
    Q_UNUSED(parent);
    return m_artistList.count();
}

QVariant ArtistModel::data(const QModelIndex& index, int role) const {
    if (index.row() < 0 || index.row() >= m_artistList.count()) {
        return QVariant();
    }

    const Artist &artist = m_artistList[index.row()];
    switch (role) {
    case IdRole:
        return artist.id();
    case NameRole:
        return artist.name();
    case SortNameRole:
        return artist.sortName();
    }

    return QVariant();
}

int ArtistModel::at(const int index) const
{
    Q_ASSERT (index >= 0 || index < m_artistList.count());
    return m_artistList.at(index).id();
}

QHash<int, QByteArray> ArtistModel::roleNames() const {
    QHash<int, QByteArray> roles;
    roles[IdRole] = "id";
    roles[NameRole] = "name";
    roles[SortNameRole] = "sortname";
    return roles;
}
