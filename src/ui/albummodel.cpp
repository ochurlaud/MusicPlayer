#include "albummodel.h"

#include <QtQml>

#include "databasemanager.h"

AlbumModel::AlbumModel(QObject* parent)
    : QAbstractListModel(parent)
{
    DatabaseManager dm;
    QList<Album> albumList = dm.getAllAlbums(DB::Album::Artist);
    beginInsertRows(QModelIndex(), m_albumList.count(), m_albumList.count());
    m_albumList << albumList;
    endInsertRows();
}

int AlbumModel::rowCount(const QModelIndex& parent) const
{
    Q_UNUSED(parent);
    return m_albumList.count();
}

QVariant AlbumModel::data(const QModelIndex& index, int role) const {
    if (index.row() < 0 || index.row() >= m_albumList.count()) {
        return QVariant();
    }

    const Album &album = m_albumList[index.row()];
    switch (role) {
    case IdRole:
        return album.id();
    case NameRole:
        return album.name();
    case SortNameRole:
        return album.sortName();
    }

    return QVariant();
}

void AlbumModel::setCurrentIndex(const int index)
{
    Q_ASSERT (index >= 0 || index < m_albumList.count());

    Q_EMIT newCurrentItemIs(m_albumList.at(index));
    m_currentIndex = index;
}

int AlbumModel::currentIndex() const
{
    return m_currentIndex;
}

int AlbumModel::at(const int index) const
{
    Q_ASSERT (index >= 0 || index < m_albumList.count());
    return m_albumList.at(index).id();
}

QHash<int, QByteArray> AlbumModel::roleNames() const {
    QHash<int, QByteArray> roles;
    roles[IdRole] = "id";
    roles[NameRole] = "name";
    roles[SortNameRole] = "sortname";
    return roles;
}
