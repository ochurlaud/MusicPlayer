#ifndef TRACKMODEL
#define TRACKMODEL

#include <QAbstractListModel>

#include "track.h"


class TrackModel : public QAbstractListModel
{
    Q_OBJECT
    Q_PROPERTY(int listId READ listId WRITE setListId NOTIFY listIdChanged)
    Q_PROPERTY(int selectType READ selectType WRITE setSelectType)

public:
    enum SelectType {
        SelectArtists = 1,
        SelectAlbums,
        SelectGenres,
        SelectPlaylists
    };

    Q_ENUMS(SelectType)

    enum TrackRoles {
        IdRole = Qt::UserRole + 1,
        NameRole,
        SortNameRole
    };

    explicit TrackModel(QObject* parent = 0);
    QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const;
    int rowCount(const QModelIndex& parent = QModelIndex()) const;
    void setListId(const int id);
    int listId() const;
    void setSelectType(const int selectType);
    int selectType() const;
    Q_INVOKABLE int at(const int index) const;
    Q_INVOKABLE void add(const QList<int>& idList);


Q_SIGNALS:
    void listIdChanged(const int id) const;

protected:
    QHash<int, QByteArray> roleNames() const;

private:
    void updateTrackList(const QList<int>& idList);
    QList<Track> m_trackList;
    int m_listId;
    int m_selectType;

};

#endif
