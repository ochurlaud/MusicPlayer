import QtQuick 2.5
import QtQuick.Window 2.2

import org.kde.musicplayer 1.0

Window {
    id: mainWindow
    visible: true
    width: 800;
    height: 600
    Column {
        spacing: 20
            anchors {
                fill: parent
                margins: 10
            }
        Row {
            spacing: 20
            width: parent.width
            height: parent.height - mediaPlayer.height - parent.spacing

            Column {
                id: leftColumn
                width: Math.max(120, mainWindow.width/4 )
                height: parent.height
                spacing: 10
                ComboBox {
                    id: viewSelection
                    width: parent.width
                    height: 40
                }
                SelectPanel {
                    id: selectPanel
                    width: parent.width
                    height: parent.height - viewSelection.height - parent.spacing
                }
            }
            Column {
                id: rightColumn
                width: parent.width - leftColumn.width - parent.spacing
                height: parent.height
                spacing: 10
                SearchInput {
                    id:searchInput;
                    height: 40;
                    width: parent.width
                }
                TrackPanel {
                    id: trackPanel
                    width: parent.width;
                    height: (rightColumn.height-2*parent.spacing-searchInput.height) - currentPlaylistPanel.height
                }
                Rectangle {
                    id: currentPlaylistPanel
                    height: currentPlaylistView.height + currentPlaylistToggler.height
                    width: parent.width
                    Column {
                        anchors.fill: parent
                        Text {
                            id: currentPlaylistToggler
                            height: 20
                            text: "%1 Song Queue".arg((currentPlaylistView.visible) ?  "\u25BC" : "\u25B6")
                            MouseArea {
                                anchors.fill: parent
                                onClicked: {
                                    currentPlaylistView.visible = !currentPlaylistView.visible
                                }
                            }
                        }
                        TrackPanel { // This should not be a TrackPanel but inherite from it (add should insert to db,...)
                            id: currentPlaylistView
                            width: parent.width;
                            height: visible ? ((rightColumn.height-2*rightColumn.spacing-searchInput.height)*1/3 - currentPlaylistToggler.height) : 0
                            color: "red"
                        }
                    }
                }
            }
        }
        MediaControls {
            id: mediaPlayer
            width: parent.width;
        }
    }
    Connections {
        target: viewSelection
        onCurrentIndexChanged: {
            changeModelType(viewSelection.modelData.get(viewSelection.currentIndex).key)
        }
    }
    Connections {
        target: selectPanel.listview
        onCurrentItemChanged: {
            trackPanel.listview.model.listId = selectPanel.listview.model.at(selectPanel.listview.currentIndex)
        }
    }
    Connections {
        target: trackPanel.listview
        onDoubleClickedTrack: {
            currentPlaylistView.listview.model.add([trackPanel.listview.model.at(trackPanel.listview.currentIndex)])
        }
    }
    Connections {
        target: currentPlaylistView.listview
        onDoubleClickedTrack: {
            console.debug(trackPanel.listview.model.at(trackPanel.listview.currentIndex))
        }
    }
    Connections {
        target: searchInput.input
        onAccepted: {
            console.debug(searchInput.input.text)
        }
    }

    function changeModelType(key) {
        if (key == "Albums") {
            trackPanel.listview.model.selectType = TrackModel.SelectAlbums
            selectPanel.listview.model = selectPanel.albumModel
        } else if (key == "Artists") {
            trackPanel.listview.model.selectType = TrackModel.SelectArtists
            selectPanel.listview.model = selectPanel.artistModel
        } else {
            grid.listview.model.selectType = TrackModel.SelectAlbums
            selectPanel.listview.model = selectPanel.albumModel
        }
    }
}
