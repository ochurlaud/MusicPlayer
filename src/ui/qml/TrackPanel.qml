import QtQuick 2.5
import QtQuick.Controls 1.1
import org.kde.musicplayer 1.0

Rectangle {
    property alias listview : listview

    Component {
        id: listDelegate
        Rectangle {
            id: delegateItem
            width: parent.width; height: 50
            color: listview.currentIndex == index ? "transparent" : "#f0f0f0"
            Row {
                anchors.fill: parent
                spacing: 5
                Rectangle {
                    width: parent.height-5; height: parent.height-5
                    anchors.verticalCenter: parent.verticalCenter
                    color: "blue"
                }
                Text { text: '<b>'+ name +'</b>' }
                Text { text: 'Nb of song' }
            }

            MouseArea {
                hoverEnabled:true
                anchors.fill: parent
                onClicked: listview.currentIndex = index
                onDoubleClicked: listview.doubleClickedTrack(index)
            }
        }
    }

    ListView {
        id: listview
        signal doubleClickedTrack(int currentIndex)
        width: parent.width;
        height: parent.height
        anchors.fill: parent
        spacing: 2
        clip: true
        model:  TrackModel { id: trackModel; listId: 0 }
        delegate: listDelegate
        highlight: Rectangle { color: "lightsteelblue" }
        highlightMoveDuration: 2
        highlightResizeDuration: 0
        MouseArea {
            hoverEnabled : true
            propagateComposedEvents: true
            anchors.fill: parent
            onEntered: parent.focus = true
            onExited: parent.focus = false
        }
    }
}

