import QtQuick 2.5
import org.kde.musicplayer 1.0

Item {
    id: wrapper
    property alias listview : listview
    property alias artistModel : artistModel
    property alias albumModel : albumModel

    Component {
        id: listDelegate
        Rectangle {
            width: parent.width
            height: 40
            color: listview.currentIndex == index ? "transparent" : "#f0f0f0"
            Row {
                Rectangle {
                    width: 25; height: 25
                    color: "blue"
                }
                Column {
                    Text { text: '<b>'+ name +'</b>' }
                    Text { text: 'Nb of song' }
                }
            }

            MouseArea {
                anchors.fill: parent
                onClicked: listview.currentIndex = index
            }
        }
    }

    ListView {
        id: listview
        width: parent.width;
        height: parent.height
        anchors.fill: parent
        spacing: 2
        clip: true
        model: albumModel
        delegate: listDelegate
        highlight: Rectangle { color: "lightsteelblue"}
        highlightMoveDuration: 2
        highlightResizeDuration: 0
        MouseArea {
            hoverEnabled : true
            propagateComposedEvents: true
            anchors.fill: parent
            onEntered: parent.focus = true
            onExited: parent.focus = false
        }
    }

    AlbumModel { id: albumModel}
    ArtistModel { id: artistModel}
}
