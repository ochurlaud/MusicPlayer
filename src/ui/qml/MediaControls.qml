import QtQuick 2.5
import QtMultimedia 5.6
import QtQuick.Controls 2.0

import org.kde.musicplayer 1.0 as MP

Rectangle {
    id: root
    implicitHeight: 60
    color: "red"

    MP.AudioController {
        id: audioController
    }
    Row {
        spacing: 0
        anchors.fill: parent
        MediaButton {
            rsize: 0.8
            source: "media-skip-backward"
        }
        MediaButton {
            id: playButton
            rsize: 1.1
            source: "media-playback-start"
            onClicked: { source = (source == "media-playback-start") ? "media-playback-pause" : "media-playback-start" }
        }
        MediaButton {
            rsize: 0.8
            source: "media-skip-forward"
        }

        Column {
            height: parent.height
            width: parent.width - 3*parent.height
            spacing: 5
            Item {
                anchors.topMargin: 10
                width: parent.width
                height: 20
                Text {
                    id: timeFrom
                    anchors.left: parent.left
                    text: "0:00"
                }

                Text {
                    anchors.centerIn: parent
                    text: "Song"
                }
                Text {
                    horizontalAlignment: Text.AlignRight
                    anchors.right: parent.right
                    text: "2:56"
                }
            }
            ProgressBar {
                id: progressBar;
                value: 0
                from: 0
                to: 200000
                width: parent.width
                height: 20
            }
        }
    }
    Connections {
        target: playButton
        onClicked: audioController.play()
    }
    Connections {
        target: audioController
        onTimeChanged: {
            progressBar.value = time
            timeFrom.text = (time/1000/60 |0) + ":" + twoDigits(time/1000%60|0)
        }
    }
    function twoDigits(number) {
        var twodigit = number >= 10 ? number : "0"+number.toString();
        return twodigit;
}
}
