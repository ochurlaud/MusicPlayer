import QtQuick 2.5
import QtQuick.Controls 2.0

ComboBox {
    id: control
    property alias modelData : modelData
    textRole: "text"
    model: ListModel {
        id: modelData
        ListElement { text: "Albums"; key: "Albums" }
        ListElement { text: "Artists"; key: "Artists" }
        ListElement { text: "Genres"; key: "Genres" }
        ListElement { text: "Playlists"; key: "Playlists" }
    }
    delegate: ItemDelegate {
        width: control.width
        height: control.height
        text: control.textRole ? (Array.isArray(control.model) ? modelData[control.textRole] :  model[control.textRole]) : modelData
        //checkable: true
        autoExclusive: true
        //checked: control.currentIndex === index
        highlighted: control.highlightedIndex === index
        //pressed: highlighted && control.pressed
    }
}
