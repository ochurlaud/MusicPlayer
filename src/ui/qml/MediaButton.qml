import QtQuick.Controls 2.0
import org.kde.plasma.core 2.0 as PlasmaCore


Button {
    property double rsize
    property alias source: image.source

    anchors.verticalCenter: parent.verticalCenter
    height: parent.height * rsize
    width: parent.height * rsize
    background: PlasmaCore.IconItem {
        id: image
        height: parent.height
        width: parent.height
    }
}
