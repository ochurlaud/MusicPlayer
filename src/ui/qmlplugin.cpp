#include "qmlplugin.h"

#include "audiocontroller.h"
#include "albummodel.h"
#include "artistmodel.h"
#include "trackmodel.h"

void QmlPlugin::registerTypes(const char *uri)
{
    Q_ASSERT(uri == QLatin1String("org.kde.musicplayer"));

    qmlRegisterType<AlbumModel>(uri, 1, 0, "AlbumModel");
    qmlRegisterType<ArtistModel>(uri, 1, 0, "ArtistModel");
    qmlRegisterType<TrackModel>(uri, 1, 0, "TrackModel");
    qmlRegisterType<AudioController>(uri, 1, 0, "AudioController");
}
