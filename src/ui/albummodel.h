#ifndef ALBUMMODEL
#define ALBUMMODEL

#include <QAbstractListModel>

#include "album.h"

class AlbumModel : public QAbstractListModel
{
    Q_OBJECT

public:
    enum AlbumRoles {
        IdRole = Qt::UserRole + 1,
        NameRole,
        SortNameRole
    };

    explicit AlbumModel(QObject* parent = 0);
    QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const;
    int rowCount(const QModelIndex& parent = QModelIndex()) const;
    void setCurrentIndex(const int index);
    int currentIndex() const;
    Q_INVOKABLE int at(const int index) const;

Q_SIGNALS:
    void newCurrentItemIs(const Album& album) const;
    void currentIndexChanged(const int index) const;

protected:
    QHash<int, QByteArray> roleNames() const;

private:
    QList<Album> m_albumList;
    int m_currentIndex;

};

#endif
