#ifndef ARTISTMODEL
#define ARTISTMODEL

#include <QAbstractListModel>

#include "artist.h"

class ArtistModel : public QAbstractListModel
{
    Q_OBJECT

public:
    enum ArtistRoles {
        IdRole = Qt::UserRole + 1,
        NameRole,
        SortNameRole
    };

    explicit ArtistModel(QObject* parent = 0);
    QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const;
    Q_INVOKABLE int at(const int index) const;
    int rowCount(const QModelIndex& parent = QModelIndex()) const;

protected:
    QHash<int, QByteArray> roleNames() const;

private:
    QList<Artist> m_artistList;

};

#endif
