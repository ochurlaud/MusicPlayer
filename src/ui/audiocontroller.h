#ifndef AUDIOCONTROLLER_H
#define AUDIOCONTROLLER_H

#include <VLCQtCore/MediaPlayer.h>
#include <VLCQtCore/Media.h>
#include <VLCQtCore/Instance.h>

#include <QtQml>
#include <QList>

#include "track.h"
#include "databasemanager.h"

class AudioController : public QObject
{
    Q_OBJECT
    //Q_PROPERTY(track READ track)
public:
    explicit AudioController(QObject* parent=0) : QObject(parent), m_media(Q_NULLPTR)
    {
        m_vlcInstance = new VlcInstance(QStringList(), this);
        m_player = new VlcMediaPlayer(m_vlcInstance);
        connect(m_player, &VlcMediaPlayer::timeChanged, this, &AudioController::onPlayerTimeChanged );
        DatabaseManager dm;
        setCurrentTrack(dm.getAllTracks().at(1));
    };

    ~AudioController(){
        m_player->stop();
        m_media->deleteLater();
        m_player->deleteLater();
        m_vlcInstance->deleteLater();
    }
    void setCurrentTrack(const Track& track) {
        m_track = track;
        m_media = new VlcMedia(track.url().toString(), m_vlcInstance);
        m_player->open(m_media);
    }

    Q_INVOKABLE void addTrack(const Track& track, int id) {
        m_playlist.insert(id, track);
    }
    Q_INVOKABLE void addTrack(const Track& track) {
        m_playlist.append(track);
    }
    Q_INVOKABLE void removeTrack(int id) {
        if (id >= m_playlist.count() || id < 0) {
            return;
        }

        m_playlist.removeAt(id);
    }
    Q_INVOKABLE void play() { m_player->play(); }
    Q_INVOKABLE void pause() { }
    Track track() {return m_track;}

public Q_SLOTS:
    void onPlayerTimeChanged(int time) { emit timeChanged(time);}

Q_SIGNALS:
    void timeChanged(int time);

private:
    VlcInstance* m_vlcInstance;
    VlcMediaPlayer* m_player;
    VlcMedia* m_media;
    QList<Track> m_playlist;
    Track m_track;

};

#endif // AUDIOCONTROLLER_H
