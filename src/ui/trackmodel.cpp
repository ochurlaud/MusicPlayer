#include "trackmodel.h"

#include <QtQml>

#include "databasemanager.h"

TrackModel::TrackModel(QObject* parent)
    : QAbstractListModel(parent)
{
    this->updateTrackList(QList<int>());
}

int TrackModel::rowCount(const QModelIndex& parent) const
{
    Q_UNUSED(parent);
    return m_trackList.count();
}

QVariant TrackModel::data(const QModelIndex& index, int role) const {
    if (index.row() < 0 || index.row() >= m_trackList.count()) {
        return QVariant();
    }
    const Track &track = m_trackList[index.row()];
    switch (role) {
    case IdRole:
        return track.id();
    case NameRole:
        return track.name();
    case SortNameRole:
        return track.sortName();
    }

    return QVariant();
}

void TrackModel::setListId(const int id)
{
    m_listId = id;
    this->updateTrackList(QList<int>({id}));
}

int TrackModel::listId() const
{
    return m_listId;
}

void TrackModel::setSelectType(const int type)
{
    m_selectType = type;
}

int TrackModel::selectType() const
{
    return m_selectType;
}

void TrackModel::updateTrackList(const QList<int>& idList)
{
    beginRemoveRows(QModelIndex(), 0, m_trackList.count());
    m_trackList.clear();
    endRemoveRows();

    DatabaseManager dm;
    switch (m_selectType) {
    case SelectArtists:
        m_trackList = dm.getTracksByArtists(idList);
        break;
    case SelectAlbums:
        m_trackList = dm.getTracksByAlbums(idList);
        break;
    default:
        m_trackList = dm.getTracksByArtists(idList);
        break;
    }
    beginInsertRows(QModelIndex(), 0, m_trackList.count()-1);
    endInsertRows();
}

int TrackModel::at(int index) const
{
    Q_ASSERT (index >= 0 || index < m_trackList.count());
    return m_trackList.at(index).id();
}

void TrackModel::add(const QList<int>& idList)
{
    beginRemoveRows(QModelIndex(), m_trackList.count(), m_trackList.count());
    DatabaseManager dm;
    m_trackList.append(dm.getTracksById(idList));
    endInsertRows();
}

QHash<int, QByteArray> TrackModel::roleNames() const {
    QHash<int, QByteArray> roles;
    roles[IdRole] = "id";
    roles[NameRole] = "name";
    roles[SortNameRole] = "sortname";
    return roles;
}
