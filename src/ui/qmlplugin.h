#ifndef ARTISTMODELPLUGIN
#define ARTISTMODELPLUGIN

#include <QtQml>

class QmlPlugin : public QQmlExtensionPlugin
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "org.kde.musicplayer");
public:
    void registerTypes(const char *uri);
};

#endif
