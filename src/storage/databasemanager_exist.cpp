#include "databasemanager.h"
#include "private/databasemanager_p.h"
#include <QSqlError>
#include <QSqlQuery>
#include <QVariant>

#include "dblogger.h"
#include "mappings.h"

int DatabaseManager::existAlbum(const Album& album)
{
    QSqlQuery query(d->db());

    query.prepare("SELECT " + DB::albumMap[DB::Album::Id] + ' ' +
                  "FROM " + DB::Tables::albums + ' ' +
                  "WHERE " + DB::albumMap[DB::Album::Artist] + "= ("
                        "SELECT " + DB::artistMap[DB::Artist::Id] + ' ' +
                        "FROM " + DB::Tables::artists + ' ' +
                        "WHERE " + DB::artistMap[DB::Artist::Name] + "=:artistn_name )"
                  "AND " + DB::albumMap[DB::Album::Name] + "=:album_name");

    query.addBindValue(album.artist().name());
    query.addBindValue(album.name());

    if (!query.exec()) {
        DB::error(_ME_, query);
        return 0;
    }

    qDebug() << query.lastQuery() << album.artist().id() << ' ' << album.name();
    if (query.next()) {

        return query.value(0).toInt();
    } else {
        return 0;
    }
}

int DatabaseManager::existArtist(const Artist& artist)
{
    QSqlQuery query(d->db());

    query.prepare("SELECT " + DB::artistMap[DB::Artist::Id] + ' ' +
                  "FROM " + DB::Tables::artists + ' ' +
                  "WHERE " + DB::artistMap[DB::Artist::Name] + "=?");

    query.addBindValue(artist.name());

    if (!query.exec()) {
        DB::error(_ME_, query);
        return 0;
    }

    if (query.next()) {
        return query.value(0).toInt();
    } else {
        return 0;
    }
}

int DatabaseManager::existTrack(const Track& track)
{
    QSqlQuery query(d->db());

    query.prepare("SELECT " + DB::albumMap[DB::Album::Id] + ' ' +
                  "FROM " + DB::Tables::tracks + ' ' +
                  "WHERE " + DB::trackMap[DB::Track::Path] + "=:path");

    query.addBindValue(track.url().toLocalFile());

    if (!query.exec()) {
        DB::error(_ME_, query);
        return 0;
    }

    if (query.next()) {
        return query.value(0).toInt();
    } else {
        return 0;
    }
}

int DatabaseManager::existGenre(const Genre& genre)
{
    QSqlQuery query(d->db());

    query.prepare("SELECT " + DB::genreMap[DB::Genre::Id] + ' ' +
                  "FROM " + DB::Tables::genres + ' ' +
                  "WHERE " + DB::genreMap[DB::Genre::Name] + "=:name");

    query.addBindValue(genre.name());

    if (!query.exec()) {
        DB::error(_ME_, query);
        return 0;
    }

    if (query.next()) {
        return query.value(0).toInt();
    } else {
        return 0;
    }
}
