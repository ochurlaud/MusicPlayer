#ifndef DATABASEMANAGERPRIVATE_H
#define DATABASEMANAGERPRIVATE_H

#include <QSqlDatabase>
#include <QString>

class DatabaseManagerPrivate
{
public:
    explicit DatabaseManagerPrivate(const QString& dbName, const QString& dbPath);
    ~DatabaseManagerPrivate();
    const QSqlDatabase& db() { return m_db; }

private:
    bool openDb(const QString& dbName, const QString& dbPath);
    bool closeDb();

    bool createTables();
    bool createTableConfig(QSqlQuery& query);
    bool createTableAlbums(QSqlQuery& query);
    bool createTableGenres(QSqlQuery& query);
    bool createTableTracks(QSqlQuery& query);
    bool createTableArtists(QSqlQuery& query);
    bool createTableOtherTags(QSqlQuery& query);
    bool createTableAlbumsGenres(QSqlQuery& query);

    bool updateTableAlbums(QSqlQuery& query);
    bool updateTableGenres(QSqlQuery& query);
    bool updateTableTracks(QSqlQuery& query);
    bool updateTableArtists(QSqlQuery& query);

    QSqlDatabase m_db;
};

#endif // DATABASEMANAGERPRIVATE_H
