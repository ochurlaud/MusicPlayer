#include "databasemanager_p.h"

#include <QSqlQuery>
#include <QDir>

#include "dblogger.h"
#include "databasemanager.h"


DatabaseManagerPrivate::DatabaseManagerPrivate(const QString& dbName, const QString& dbPath)
{
    this->openDb(dbName, dbPath);
    this->createTables();
}

DatabaseManagerPrivate::~DatabaseManagerPrivate()
{
    this->closeDb();
}

bool DatabaseManagerPrivate::openDb(const QString& dbName, const QString& dbPath)
{
    if (QSqlDatabase::contains(dbName)) {
        m_db = QSqlDatabase::database(dbName);
    } else {
        m_db = QSqlDatabase::addDatabase("QSQLITE", dbName);
    }

    QDir appDataDir(dbPath);
    if (!appDataDir.exists()) {
        appDataDir.mkpath(dbPath);
    }

    QString fullPath = dbPath + '/' + dbName + ".sqlite";
    m_db.setDatabaseName(fullPath);

    if (!m_db.open()) {
        debug(_ME_) << "Database unable to open.";
        return false;
    }

    debugIn(_ME_) << "Database opened in: " + fullPath;

    QSqlQuery query(m_db);
    query.prepare("PRAGMA foreign_keys = ON");

    if (!query.exec()) {
        DB::error(_ME_, query);
        return false;
    }

    return true;
}

bool DatabaseManagerPrivate::closeDb()
{
    debugOut(_ME_) << "Close DB";
    m_db.close();

    return true;
}

bool DatabaseManagerPrivate::createTables()
{
    debugIn(_ME_) << "Create tables";

    bool ret = false;

    if (m_db.isOpen()) {
        QSqlQuery query(m_db);
        ret = query.exec("PRAGMA foreign_keys = ON");

        if (m_db.tables().contains("config")) {
            debug(_ME_) << "config table exists";
            query.exec("SELECT db_version FROM config");
            query.next();
            if (query.value(0).toInt() != DB_VERSION) {
                //ret = upgradeDB(query.value(0).toInt(), DB_VERSION);
            }
        } else { //if config table do not exists then the db is empty...
            debug(_ME_) << "configTable does not exist";

            ret &= this->createTableArtists(query);
            ret &= this->createTableAlbums(query);
            ret &= this->createTableGenres(query);
            ret &= this->createTableAlbumsGenres(query);
            ret &= this->createTableTracks(query);
            ret &= this->createTableOtherTags(query);

            if (ret) {
                ret &= createTableConfig(query);
            }
        }
    }
    debugOut(_ME_) << "Done";
    return ret;
}

bool DatabaseManagerPrivate::createTableConfig(QSqlQuery& query)
{
    debug(_ME_)  << "Create Table 'config'";
    query.prepare("CREATE TABLE IF NOT EXISTS " + DB::Tables::config + "("
                  + DB::configMap[DB::Config::DB_Version] + " INTEGER"
                  ")");

    if (!query.exec()) {
        DB::error(_ME_, query);
        return false;
    }

    // Set the database version
    query.prepare("INSERT INTO " + DB::Tables::config + "("
                  + DB::configMap[DB::Config::DB_Version] +
                  ") VALUES (" + QString::number(DB_VERSION) + ")");

    if (!query.exec()) {
        DB::error(_ME_, query);
        return false;
    }

    return true;
}

bool DatabaseManagerPrivate::createTableGenres(QSqlQuery& query)
{
    debug(_ME_) << "Create Table 'genres'";
    query.prepare("CREATE TABLE IF NOT EXISTS " + DB::Tables::genres + "("
                  + DB::genreMap[DB::Genre::Id] + " INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE, "
                  + DB::genreMap[DB::Genre::Name] + " VARCHAR, "
                  "UNIQUE (" + DB::genreMap[DB::Genre::Name] + " ) "
                  "ON CONFLICT IGNORE"
                  ")");

    if (!query.exec()) {
        DB::error(_ME_, query);

        return false;
    }

    return true;
}

bool DatabaseManagerPrivate::createTableAlbumsGenres(QSqlQuery& query)
{
    debug(_ME_) << "Create Table 'albums_genres'";
    query.prepare("CREATE TABLE IF NOT EXISTS " + DB::Tables::albumsGenres + "("
                  + DB::albumGenreMap[DB::AlbumGenre::Id] + " INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE, "
                  + DB::albumGenreMap[DB::AlbumGenre::Album] + " INTEGER NOT NULL, "
                  + DB::albumGenreMap[DB::AlbumGenre::Genre] + " INTEGER NOT NULL, "
                  "UNIQUE (" + DB::albumGenreMap[DB::AlbumGenre::Album] + ", "
                  + DB::albumGenreMap[DB::AlbumGenre::Genre] + " ) "
                  "ON CONFLICT IGNORE, "
                  "FOREIGN KEY (" + DB::albumGenreMap[DB::AlbumGenre::Album] + ") "
                  "REFERENCES " + DB::Tables::albums + "(" + DB::albumMap[DB::Album::Id] + "), "
                  "FOREIGN KEY (" + DB::albumGenreMap[DB::AlbumGenre::Genre] + ") "
                  "REFERENCES " + DB::Tables::genres + "(" + DB::genreMap[DB::Genre::Id] + ")"
                  ")");

    if (!query.exec()) {
        DB::error(_ME_, query);

        return false;
    }

    return true;
}

bool DatabaseManagerPrivate::createTableAlbums(QSqlQuery& query)
{
    qDebug() << "Create Table 'albums'";
    query.prepare("CREATE TABLE IF NOT EXISTS " + DB::Tables::albums + "("
                  + DB::albumMap[DB::Album::Id] + " INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE, "
                  + DB::albumMap[DB::Album::Name] + " VARCHAR NOT NULL, "
                  + DB::albumMap[DB::Album::SortName] + " VARCHAR, "
                  + DB::albumMap[DB::Album::NumberOfTracks] + " INTEGER, "
                  + DB::albumMap[DB::Album::DiscNumber] + " INTEGER, "
                  + DB::albumMap[DB::Album::NumberOfDiscs] + " INTEGER, "
                  + DB::albumMap[DB::Album::Artist] + " INTEGER, "
                  + DB::albumMap[DB::Album::Year] + " INTEGER, "
                  + DB::albumMap[DB::Album::CoverPath] + " VARCHAR, "
                  "UNIQUE (" + DB::albumMap[DB::Album::Artist] + ", "
                  + DB::albumMap[DB::Album::Name] + ") "
                  "ON CONFLICT IGNORE, "
                  "FOREIGN KEY (" + DB::albumMap[DB::Album::Artist] + ") "
                  "REFERENCES " + DB::Tables::artists + "(" + DB::artistMap[DB::Artist::Id] + ")"
                  ")");


    if (!query.exec()) {
        DB::error(_ME_, query);

        return false;
    }

    return true;
}

bool DatabaseManagerPrivate::createTableOtherTags(QSqlQuery& query)
{
    qDebug() << "Create table 'Other Tags'";
    query.prepare("CREATE TABLE IF NOT EXISTS " + DB::Tables::otherTags + "("
                  + DB::otherTagMap[DB::OtherTag::Id] + " INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE, "
                  + DB::otherTagMap[DB::OtherTag::Name] + " VARCHAR NOT NULL, "
                  + DB::otherTagMap[DB::OtherTag::Value] + " VARCHAR NOT NULL, "
                  + DB::otherTagMap[DB::OtherTag::Track] + " INT, "
                  "UNIQUE (" + DB::otherTagMap[DB::OtherTag::Value] + ", "
                  + DB::otherTagMap[DB::OtherTag::Track] + ") "
                  "ON CONFLICT IGNORE, "
                  "FOREIGN KEY (" + DB::otherTagMap[DB::OtherTag::Track] + ") "
                  "REFERENCES " + DB::Tables::tracks + "(" + DB::trackMap[DB::Track::Id] + ")"
                  ")");

    if (!query.exec()) {
        DB::error(_ME_, query);

        return false;
    }

    return true;
}

bool DatabaseManagerPrivate::createTableTracks(QSqlQuery& query)
{
    qDebug() << "Create Table 'tracks'";
    query.prepare("CREATE TABLE IF NOT EXISTS " + DB::Tables::tracks + "("
                  + DB::trackMap[DB::Track::Id] + " INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE, "
                  + DB::trackMap[DB::Track::Name] + " VARCHAR NOT NULL, "
                  + DB::trackMap[DB::Track::SortName] + " VARCHAR, "
                  + DB::trackMap[DB::Track::TrackNumber] + " INTEGER, "
                  + DB::trackMap[DB::Track::Artist] + " INTEGER, "
                  + DB::trackMap[DB::Track::Composer] + " INTEGER, "
                  + DB::trackMap[DB::Track::Conductor] + " INTEGER, "
                  + DB::trackMap[DB::Track::Performer] + " INTEGER, "
                  + DB::trackMap[DB::Track::Album] + " INTEGER, "
                  + DB::trackMap[DB::Track::EncodedBy] + " VARCHAR, "
                  + DB::trackMap[DB::Track::Playcount] + " INTEGER, "
                  + DB::trackMap[DB::Track::Comment] + " VARCHAR, "
                  + DB::trackMap[DB::Track::UserRate] + " INTEGER, "
                  + DB::trackMap[DB::Track::Format] + " VARCHAR, "
                  + DB::trackMap[DB::Track::Duration] + " INTEGER, "
                  + DB::trackMap[DB::Track::BPM] + " INTEGER, "
                  + DB::trackMap[DB::Track::SampleRate] + " INTEGER, "
                  + DB::trackMap[DB::Track::ChannelNumber] + " INTEGER, "
                  + DB::trackMap[DB::Track::Bitrate] + " INTEGER, "
                  + DB::trackMap[DB::Track::Path] + " VARCHAR UNIQUE, "
                  "FOREIGN KEY (" + DB::trackMap[DB::Track::Artist] + ") "
                  "REFERENCES " + DB::Tables::artists + "(" + DB::artistMap[DB::Artist::Id] + "), "
                  "FOREIGN KEY (" + DB::trackMap[DB::Track::Composer] + ") "
                  "REFERENCES " + DB::Tables::artists + "(" + DB::artistMap[DB::Artist::Id] + "), "
                  "FOREIGN KEY (" + DB::trackMap[DB::Track::Conductor] + ") "
                  "REFERENCES " + DB::Tables::artists + "(" + DB::artistMap[DB::Artist::Id] + "), "
                  "FOREIGN KEY (" + DB::trackMap[DB::Track::Performer] + ") "
                  "REFERENCES " + DB::Tables::artists + "(" + DB::artistMap[DB::Artist::Id] + "), "
                  "FOREIGN KEY (" + DB::trackMap[DB::Track::Album] + ") "
                  "REFERENCES " + DB::Tables::albums + "(" + DB::albumMap[DB::Album::Id] + ")"
                  ")");

    if (!query.exec()) {
        DB::error(_ME_, query);

        return false;
    }

    return true;
}

bool DatabaseManagerPrivate::createTableArtists(QSqlQuery& query)
{
    qDebug() << "Create Table 'artists'";

    query.prepare("CREATE TABLE IF NOT EXISTS " + DB::Tables::artists + "("
                  + DB::artistMap[DB::Artist::Id] + " INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE, "
                  + DB::artistMap[DB::Artist::Name] + " VARCHAR NOT NULL UNIQUE, "
                  + DB::artistMap[DB::Artist::SortName] + " VARCHAR, "
                  + DB::artistMap[DB::Artist::Type] + " INTEGER, "
                  "UNIQUE (" + DB::artistMap[DB::Artist::Name] + " ) "
                  "ON CONFLICT IGNORE"
                  ")");

    if (!query.exec()) {
        DB::error(_ME_, query);

        return false;
    }

    return true;
}
