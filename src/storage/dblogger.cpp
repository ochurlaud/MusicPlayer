#include "dblogger.h"

#include <QSqlError>

void DB::error(const QString& src, const QSqlQuery& query) {

    debug(src) << query.lastError().text();
    debug() << query.lastQuery();
}
