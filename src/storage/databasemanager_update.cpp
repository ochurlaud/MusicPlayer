#include "databasemanager.h"
#include "private/databasemanager_p.h"

#include <QSqlQuery>
#include <QVariant>

#include "dblogger.h"
#include "mappings.h"

void DatabaseManager::updateTracks(QList<Track>& trackList,
                                   const QList<QPair<DB::Track::Fields, QVariant> >& elementList)
{
    QSqlQuery query(d->db());
    QString queryText = "UPDATE tracks SET ";

    Q_FOREACH (auto element, elementList) {
        switch (element.first) {
        case DB::Track::Artist:
            break;
        case DB::Track::Conductor:
            break;
        case DB::Track::Composer:
            break;
        case DB::Track::Performer:
            break;
        case DB::Track::Album:
            break;
        default:
            QString key = DB::trackMap[element.first];
            queryText.append(key + "=?");
            query.addBindValue(element.second);
        }

        if (!elementList.endsWith(element)) {
            queryText.append(',');
        }
        queryText.append(' ');
    }
    queryText.append(" WHERE id IN (");
    for (int i = 0 ; i < trackList.count() ; i++) {
        queryText.append("?");
        if (i+1 < trackList.count()) {
            queryText.append(", ");
        }
    }
    queryText.append(")");

    query.prepare(queryText);

    // Maybe one could call the addBindValues() before prepare() to have only 1 loop.
    Q_FOREACH (auto element, elementList) {
        query.addBindValue(element.second);
    }
    Q_FOREACH (auto track, trackList) {
        query.addBindValue(track.id());
    }

    query.exec();
}

void DatabaseManager::updateArtist(Artist& artist)
{
    QSqlQuery query(d->db());

    int previousType = this->getTypeFromArtist(artist);

    query.prepare("UPDATE " + DB::Tables::artists + ' ' +
                  "SET " + DB::artistMap[DB::Artist::SortName] + "=:sortname, "
                         + DB::artistMap[DB::Artist::Type] + "=:type "
                  "WHERE " + DB::artistMap[DB::Artist::Id] + "=:id");
    query.addBindValue( "dd" );
    query.addBindValue( (previousType|artist.type()) );
    query.addBindValue( artist.id() );

    if (!query.exec()) {
        DB::error(_ME_, query);
        qDebug() << query.lastQuery();
        qDebug() << artist.id() << (previousType|artist.type())  ;
    }
}

void DatabaseManager::updateAlbum(Album& album,
                                  const QList<QPair<DB::Album::Fields, QVariant> >& elementList)
{
    QSqlQuery query(d->db());
    QString queryText = "UPDATE " + DB::Tables::albums + " SET ";

    Q_FOREACH (const auto& element, elementList) {
        if (element.first == DB::Album::Artist) {
            Artist artist = element.second.value<Artist>();
            if (int id = this->existArtist(artist)) {
                artist.setId(id);
                this->updateArtist(artist);
            } else {
                insertArtist(artist);
            }
            album.setArtist(artist);
            QString key = DB::albumMap[DB::Album::Artist];
            queryText.append(key + "=?");
            query.addBindValue(artist.id());
        } else {
            QString key = DB::albumMap[element.first];
            queryText.append(key + "=?");
            query.addBindValue(element.second);
        }
        if (!elementList.endsWith(element)) {
            queryText.append(',');
        }
        queryText.append(' ');
    }

    queryText.append("WHERE id=?");
    query.addBindValue(album.id());

    query.prepare(queryText);

    if (!query.exec()) {
        DB::error(_ME_, query);
    } else {
    }
}

