#include "databasemanager.h"
#include "private/databasemanager_p.h"

#include <QDir>
#include <QStandardPaths>
#include <QSqlError>
#include <QSqlQuery>

#include "dblogger.h"
#include "mappings.h"


DatabaseManager::DatabaseManager(QString dbName, QString dbPath, QObject *parent)
    : QObject(parent)
    , d(new DatabaseManagerPrivate(dbName, dbPath))
{}

DatabaseManager::~DatabaseManager()
{
    delete d;
}
