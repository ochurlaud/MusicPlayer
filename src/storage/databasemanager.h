#ifndef DATABASEMANAGER_H
#define DATABASEMANAGER_H

#include <QObject>
#include <QList>
#include <QSqlDatabase>
#include <QStandardPaths>

// Model
#include "track.h"
#include "artist.h"

//Storage
#include "musicplayer_storage_export.h"
#include "mappings.h"

#define DB_VERSION 001

class DatabaseManagerPrivate;

class MUSICPLAYER_STORAGE_EXPORT DatabaseManager : public QObject
{
    Q_OBJECT
public:
    /**
     * Should be called with:
     * @param dbName = the name of your lib
     * @param dbPath = QStandardPaths::writableLocation(QStandardPaths::AppDataLocation)

     *
     * Maybe we could define it as default parameters?
     */
    explicit DatabaseManager(QString dbName = "library",
                             QString dbPath = QStandardPaths::writableLocation(QStandardPaths::AppDataLocation),
                             QObject* parent = 0);
    ~DatabaseManager();

    // This should really be shown in the lib
    void updateTracks(QList< Track > &trackList, const QList< QPair< DB::Track::Fields, QVariant > > &elementList);
    void updateArtist(Artist& artist);
    void updateAlbum(Album &album, const QList< QPair< DB::Album::Fields, QVariant > > &elementList);

    int existAlbum(const Album&);
    int existArtist(const Artist&);
    int existTrack(const Track&);
    int existGenre(const Genre&);

    void insertTracks(QList<Track>& trackList);
    void insertAlbum(Album& album);
    void insertArtist(Artist& artist);
    void insertGenres(QList<Genre>& genreList, Album& album);
    void linkGenresToAlbum(QList<Genre>& genreList, Album& album);
    void insertOtherTags(const QList< QPair< QString, QString > > &otherTags, const Track &track);

    int getTypeFromArtist(const Artist& artist);
    QList<Album> getAllAlbums(const DB::Album::Fields order=DB::Album::Name);
    QList<Track> getAllTracks(const DB::Track::Fields order=DB::Track::Name);
    QList<Track> getTracksById(const QList<int>& idList, const DB::Track::Fields order=DB::Track::Name);
    QList<Track> getTracksByArtists(const QList<int>& idList,const DB::Track::Fields order=DB::Track::Name);
    QList<Track> getTracksByAlbums(const QList<int>& idList,const DB::Track::Fields order=DB::Track::Name);

    QList<Artist> getAllArtists(const int type, const DB::Artist::Fields order=DB::Artist::Name);

private:
    DatabaseManagerPrivate *d;

    friend class DatabaseManagerTest;
};

#endif // DATABASEMANAGER_H
