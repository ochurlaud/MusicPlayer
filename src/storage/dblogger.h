#ifndef DBLOGGER_H
#define DBLOGGER_H

#include <QDebug>
#include <QSqlQuery>

#include "logger.h"

#define _ME_ __PRETTY_FUNCTION__

namespace DB {

void error(const QString& src, const QSqlQuery& query);

}
#endif // DBLOGGER_H
