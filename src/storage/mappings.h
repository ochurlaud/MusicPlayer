#ifndef MAPS_H
#define MAPS_H

#include <QHash>
#include "enums.h"

namespace DB {

    namespace Tables {
        const QString config("config");
        const QString albums("albums");
        const QString artists("artists");
        const QString genres("genres");
        const QString tracks("tracks");
        const QString albumsGenres("albums_genres");
        const QString otherTags("other_tags");
    };

    const QHash<const Track::Fields, QString> trackMap ({
        {Track::Id, "id"},
        {Track::Name, "title"},
        {Track::SortName, "title_sort"},
        {Track::TrackNumber, "track_nb"},
        {Track::Artist, "artist_id"},
        {Track::Composer, "composer_id"},
        {Track::Conductor, "conductor_id"},
        {Track::Performer, "performer_id"},
        {Track::Album, "album_id"},
        {Track::EncodedBy, "encoded_by"},
        {Track::Playcount, "playcount"},
        {Track::Comment, "comment"},
        {Track::UserRate, "user_rate"},
        {Track::Format, "format"},
    // Track::OtherTags is not in the 'tracks' table fields
        {Track::Duration, "duration"},
        {Track::BPM, "bpm"},
        {Track::SampleRate, "sample_rate"},
        {Track::ChannelNumber, "channel_number"},
        {Track::Bitrate, "bitrate"},
        {Track::Path, "path"}
    });

    const QHash<const Album::Fields, QString> albumMap ({
        {Album::Id, "id"},
        {Album::Name, "name"},
        {Album::SortName, "name_sort"},
      // Album::Genre is not in the  'albums' table fields
        {Album::NumberOfTracks, "number_of_tracks"},
        {Album::DiscNumber, "disc_number"},
        {Album::NumberOfDiscs, "number_of_discs"},
        {Album::Artist, "artist_id"},
        {Album::Year, "year"},
        {Album::CoverPath, "cover_path"}
    });

    const QHash<const Artist::Fields, QString> artistMap ({
        {Artist::Id, "id"},
        {Artist::Name, "name"},
        {Artist::SortName, "name_sort"},
        {Artist::Type, "type"}
    });

    const QHash<const Genre::Fields, QString> genreMap ({
        {Genre::Id, "id"},
        {Genre::Name, "name"}
    });

    const QHash<const OtherTag::Fields, QString> otherTagMap ({
        {OtherTag::Id, "id"},
        {OtherTag::Name, "name"},
        {OtherTag::Value, "value"},
        {OtherTag::Track, "track_id"}
    });

    const QHash<const AlbumGenre::Fields, QString> albumGenreMap ({
        {AlbumGenre::Id, "id"},
        {AlbumGenre::Album, "album_id"},
        {AlbumGenre::Genre, "genre_id"}
    });

    const QHash<const Config::Fields, QString> configMap ({
        {Config::DB_Version, "db_version"}
    });

    inline const QString albumMapAs(Album::Fields field, const QString& prefix) {
        return prefix + '.' + albumMap[field];
    }
    inline const QString albumGenreMapAs(AlbumGenre::Fields field, const QString& prefix) {
        return prefix + '.' + albumGenreMap[field];
    }
    inline const QString artistMapAs(Artist::Fields field, const QString& prefix) {
        return prefix + '.' + artistMap[field];
    }
    inline const QString genreMapAs(Genre::Fields field, const QString& prefix) {
        return prefix + '.' + genreMap[field];
    }
    inline const QString trackMapAs(Track::Fields field, const QString& prefix) {
        return prefix + '.' + trackMap[field];
    }
}

#endif
