#ifndef ENUMS_H
#define ENUMS_H

namespace DB {

    namespace Table {
        enum Names {
            Config = 0,
            Albums,
            Artists,
            Genres,
            Tracks,
            AlbumsGenres,
            OtherTags
        };
    }

    namespace Track {
        enum Fields {
            Id = 0,
            Name,
            SortName,
            TrackNumber,
            Artist,
            Composer,
            Conductor,
            Performer,
            Album,
            EncodedBy,
            Playcount,
            Comment,
            UserRate,
            Format,
            OtherTags,
            Duration,
            BPM,
            SampleRate,
            ChannelNumber,
            Bitrate,
            Path
        };
    }

    namespace Album {
        enum Fields {
            Id = 0,
            Name,
            SortName,
            Genre,
            NumberOfTracks,
            DiscNumber,
            NumberOfDiscs,
            Artist,
            Year,
            CoverPath
        };
    }

    namespace Artist {
        enum Fields {
            Id = 0,
            Name,
            SortName,
            Type
        };
    }

    namespace Genre {
        enum Fields {
            Id = 0,
            Name
        };
    }

    // For objects that don't have any reality out of the db
    namespace OtherTag {
        enum Fields {
            Id = 0,
            Name,
            Value,
            Track
        };
    }

    namespace AlbumGenre {
        enum Fields {
            Id = 0,
            Album,
            Genre
        };
    }

    namespace Config {
        enum Fields {
            DB_Version = 0
        };
    }
}

#endif // ENUMS_H
