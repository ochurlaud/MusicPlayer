#include "databasemanager.h"
#include "private/databasemanager_p.h"

#include <QSqlQuery>
#include <QVariant>

#include "dblogger.h"
#include "mappings.h"

#include "artist.h"

int DatabaseManager::getTypeFromArtist(const Artist& artist)
{
    debug(_ME_);
    QSqlQuery query(d->db());
    query.prepare("SELECT " + DB::artistMap[DB::Artist::Type] + ' ' +
                  "FROM " + DB::Tables::artists + ' ' +
                  "WHERE " + DB::artistMap[DB::Artist::Id] +"=:id");
    query.addBindValue(artist.id());

    if(!query.exec()) {
        DB::error(_ME_, query);
    } else {
        if (query.next()) {
            return query.value(0).toInt();
        }
    }

    return ArtistType::None;
}

QList<Artist> DatabaseManager::getAllArtists(const int type,
                                             const DB::Artist::Fields order)
{
    debug(_ME_);
    QList<Artist> artistList;
    QSqlQuery query(d->db());
    query.prepare("SELECT " + DB::artistMap[DB::Artist::Id] + ", " +
                              DB::artistMap[DB::Artist::Name] + ", " +
                              DB::artistMap[DB::Artist::SortName] + ", " +
                              DB::artistMap[DB::Artist::Type] + ' ' +
                  "FROM " + DB::Tables::artists + ' ' +
                  "WHERE " + DB::artistMap[DB::Artist::Type] + "=:type " +
                  "ORDER BY " + DB::artistMap[order]);
    query.addBindValue(type);

    if(!query.exec()) {
        DB::error(_ME_, query);
    } else {
        while (query.next()) {
            Artist artist;
            artist.setId(query.value(0).toInt());
            artist.setName(query.value(1).toString());
            artist.setSortName(query.value(2).toString());
            artist.setType(query.value(3).toInt());
            artistList.append(artist);
        }
    }
    return artistList;
}

QList<Album> DatabaseManager::getAllAlbums(const DB::Album::Fields order)
{
    debug(_ME_);
    QList<Album> albumList;
    QSqlQuery query(d->db());
    query.prepare("SELECT " + DB::albumMap[DB::Album::Id] + ", " +
                              DB::albumMap[DB::Album::Name] + ", " +
                              DB::albumMap[DB::Album::SortName] + ' ' +
                  "FROM " + DB::Tables::albums + ' ' +
                  "ORDER BY " + DB::albumMap[order]);

    if(!query.exec()) {
        DB::error(_ME_, query);
    } else {
        while (query.next()) {
            Album album;
            album.setId(query.value(0).toInt());
            album.setName(query.value(1).toString());
            album.setSortName(query.value(2).toString());
            albumList.append(album);
        }
    }
    return albumList;
}

QList<Track> DatabaseManager::getAllTracks(const DB::Track::Fields order)
{
    debug(_ME_);
    QList<Track> trackList;
    QSqlQuery query(d->db());
    query.prepare("SELECT " + DB::trackMap[DB::Track::Id] + ", " +
                              DB::trackMap[DB::Track::Name] + ", " +
                              DB::trackMap[DB::Track::SortName] + ", " +
                              DB::trackMap[DB::Track::Path] + ' ' +
                  "FROM " + DB::Tables::tracks + ' ' +
                  "ORDER BY " + DB::trackMap[order]);

    if(!query.exec()) {
        DB::error(_ME_, query);
    } else {
        while (query.next()) {
            Track track;
            track.setId(query.value(0).toInt());
            track.setName(query.value(1).toString());
            track.setSortName(query.value(2).toString());
            track.setUrlFromFile(query.value(3).toString());
            trackList.append(track);
        }
    }
    return trackList;
}

QList<Track> DatabaseManager::getTracksById(const QList<int>& idList, const DB::Track::Fields order)
{
    debug(_ME_);
    if (idList.isEmpty()) {
        return getAllTracks();
    }

    QString placeholders = QString("?,").repeated(idList.count());
    placeholders.remove(placeholders.count()-1, placeholders.count());

    QList<Track> trackList;
    QSqlQuery query(d->db());
    query.prepare("SELECT " + DB::trackMap[DB::Track::Id] + ", " +
                              DB::trackMap[DB::Track::Name] + ", " +
                              DB::trackMap[DB::Track::SortName] + ", " +
                              DB::trackMap[DB::Track::Path] + ' ' +
                  "FROM " + DB::Tables::tracks + ' ' +
                  "WHERE " + DB::trackMap[DB::Track::Id] + " IN (" + placeholders + ") " +
                  "ORDER BY " + DB::trackMap[order]);

    for (const int id : idList) {
        query.addBindValue(id);
    }

    if(!query.exec()) {
        DB::error(_ME_, query);
    } else {
        while (query.next()) {
            Track track;
            track.setId(query.value(0).toInt());
            track.setName(query.value(1).toString());
            track.setSortName(query.value(2).toString());
            track.setUrlFromFile(query.value(3).toString());
            trackList.append(track);
        }
    }
    return trackList;
}

QList<Track> DatabaseManager::getTracksByArtists(const QList<int>& idList,
                                                 const DB::Track::Fields order)
{
    debug(_ME_);
    if (idList.isEmpty()) {
        return getAllTracks();
    }

    QString placeholders = QString("?,").repeated(idList.count());
    placeholders.remove(placeholders.count()-1, placeholders.count());

    QList<Track> trackList;
    QSqlQuery query(d->db());
    query.prepare("SELECT " + DB::trackMapAs(DB::Track::Id, "T") + ", " +
                              DB::trackMapAs(DB::Track::Name, "T") + ", " +
                              DB::trackMapAs(DB::Track::SortName, "T") + ", " +
                              DB::trackMapAs(DB::Track::Path, "T") + ' ' +
                  "FROM " + DB::Tables::tracks + " AS T " +
                  "LEFT JOIN " + DB::Tables::albums + " AS A " +
                        "ON " + DB::albumMapAs(DB::Album::Id, "A") +
                            "=" + DB::trackMapAs(DB::Track::Album, "T") + ' ' +
                  "WHERE " + DB::trackMapAs(DB::Track::Artist, "A") + " IN (" + placeholders + ") " +
                  "ORDER BY " + DB::trackMapAs(order, "T"));


    for (const int id : idList) {
        query.addBindValue(id);
    }

    if(!query.exec()) {
        DB::error(_ME_, query);
    } else {
        while (query.next()) {
            Track track;
            track.setId(query.value(0).toInt());
            track.setName(query.value(1).toString());
            track.setSortName(query.value(2).toString());
            track.setUrlFromFile(query.value(3).toString());
            trackList.append(track);
        }
    }

    return trackList;
}

QList<Track> DatabaseManager::getTracksByAlbums(const QList<int>& idList,
                                                const DB::Track::Fields order)
{
    if (idList.isEmpty()) {
        return getAllTracks();
    }

    QString placeholders = QString("?,").repeated(idList.count());
    placeholders.remove(placeholders.count()-1, placeholders.count());

    QList<Track> trackList;
    QSqlQuery query(d->db());
    query.prepare("SELECT " + DB::trackMapAs(DB::Track::Id, "T") + ", " +
                              DB::trackMapAs(DB::Track::Name, "T") + ", " +
                              DB::trackMapAs(DB::Track::SortName, "T") + ", " +
                              DB::trackMapAs(DB::Track::Path, "T") + ' ' +
                  "FROM " + DB::Tables::tracks + " AS T " +
                  "WHERE " + DB::trackMapAs(DB::Track::Album, "T") + " IN (" + placeholders + ") " +
                  "ORDER BY " + DB::trackMapAs(order, "T"));

    for (const int id : idList) {
        query.addBindValue(id);
    }

    if(!query.exec()) {
        DB::error(_ME_, query);
    } else {
        while (query.next()) {
            Track track;
            track.setId(query.value(0).toInt());
            track.setName(query.value(1).toString());
            track.setSortName(query.value(2).toString());
            track.setUrlFromFile(query.value(3).toString());
            trackList.append(track);
        }
    }

    return trackList;
}
