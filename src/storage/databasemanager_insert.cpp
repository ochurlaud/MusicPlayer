#include "databasemanager.h"
#include "private/databasemanager_p.h"

#include <QDebug>
#include <QSqlQuery>
#include <QSqlError>
#include <QVariant>

#include "dblogger.h"


void DatabaseManager::insertTracks(QList<Track>& trackList)
{
    QSqlQuery query(d->db());

    query.prepare("INSERT INTO " + DB::Tables::tracks + "("
        + DB::trackMap[DB::Track::Name] + ", "
        + DB::trackMap[DB::Track::SortName] + ", "
        + DB::trackMap[DB::Track::TrackNumber] + ", "
        + DB::trackMap[DB::Track::Artist] + ", "
        + DB::trackMap[DB::Track::Composer] + ", "
        + DB::trackMap[DB::Track::Conductor] + ", "
        + DB::trackMap[DB::Track::Performer] + ", "
        + DB::trackMap[DB::Track::Album] + ", "
        + DB::trackMap[DB::Track::EncodedBy] + ", "
        + DB::trackMap[DB::Track::Playcount] + ", "
        + DB::trackMap[DB::Track::Comment] + ", "
        + DB::trackMap[DB::Track::UserRate] + ", "
        + DB::trackMap[DB::Track::Format] + ", "
        + DB::trackMap[DB::Track::Duration] + ", "
        + DB::trackMap[DB::Track::BPM] + ", "
        + DB::trackMap[DB::Track::SampleRate] + ", "
        + DB::trackMap[DB::Track::ChannelNumber] + ", "
        + DB::trackMap[DB::Track::Bitrate] + ", "
        + DB::trackMap[DB::Track::Path] + ") "
        "VALUES (:name, :sortname, :tracknumber, :artist, :composer, :conductor, "
                ":performer, :album, :encodedby, :playcount, :comment, :userrate, "
                ":format, :duration, :bpm, :samplerate, :channelnumber, :bitrate, "
                ":path)");

    for (Track& track : trackList) {
        if (!this->existTrack(track)) {
            query.addBindValue(track.name());
            query.addBindValue(track.sortName());
            query.addBindValue(track.trackNumber());
            if (!track.trackArtist().name().isEmpty()) {
                Artist artist = track.trackArtist();
                this->insertArtist(artist);
                track.setTrackArtist(artist);
                query.addBindValue(track.trackArtist().id());
            } else {
                query.addBindValue(QVariant(QVariant::Int));
            }
            if (!track.composer().name().isEmpty()) {
                Artist composer = track.composer();
                this->insertArtist(composer);
                track.setComposer(composer);
                query.addBindValue(track.composer().id());
            } else {
                query.addBindValue(QVariant(QVariant::Int));
            }
            if (!track.conductor().name().isEmpty()) {
                Artist conductor = track.conductor();
                this->insertArtist(conductor);
                track.setConductor(conductor);
                query.addBindValue(track.conductor().id());
            } else {
                query.addBindValue(QVariant(QVariant::Int));
            }
            if (!track.performer().name().isEmpty()) {
                Artist performer = track.performer();
                this->insertArtist(performer);
                track.setPerformer(performer);
                query.addBindValue(track.performer().id());
            } else {
                query.addBindValue(QVariant(QVariant::Int));
            }
            if (!track.album().name().isEmpty()) {
                Album album = track.album();
                this->insertAlbum(album);
                track.setAlbum(album);
                query.addBindValue(track.album().id());
            } else {
                query.addBindValue(QVariant(QVariant::Int));
            }
            query.addBindValue(track.encodedBy());
            query.addBindValue(track.playcount());
            query.addBindValue(track.comment());
            query.addBindValue(track.userRate());
            query.addBindValue(track.format());
            query.addBindValue(track.duration());
            query.addBindValue(track.BPM());
            query.addBindValue(track.sampleRate());
            query.addBindValue(track.channelNumber());
            query.addBindValue(track.bitrate());
            query.addBindValue(track.url().toLocalFile());

            if(!query.exec()) {
                DB::error(_ME_, query);
            } else {
                track.setId(query.lastInsertId().toInt());
            }
        }
    }
}

void DatabaseManager::insertArtist(Artist& artist)
{
    if (int id = this->existArtist(artist)) {
        artist.setId(id);
        this->updateArtist(artist);
    } else {

        QSqlQuery query(d->db());
        query.prepare("INSERT INTO " + DB::Tables::artists + "("
            + DB::artistMap[DB::Artist::Name] + ", "
            + DB::artistMap[DB::Artist::SortName] + ", "
            + DB::artistMap[DB::Artist::Type] + ") "
            "VALUES (:name, :sortname, :type)");

        query.addBindValue(artist.name());
        query.addBindValue(artist.sortName());
        query.addBindValue(artist.type());

        if(!query.exec()) {
            DB::error(_ME_, query);
        } else {
            artist.setId(query.lastInsertId().toInt());
        }
    }
}

void DatabaseManager::insertAlbum(Album& album)
{
    if (int id = this->existAlbum(album)) {
        album.setId(id);
#pragma message( "UPDATE ALBUM ")
        //this->updateAlbum(album);
    } else {
        QSqlQuery query(d->db());

        query.prepare("INSERT INTO " + DB::Tables::albums + "("
            + DB::albumMap[DB::Album::Name] + ", "
            + DB::albumMap[DB::Album::SortName] + ", "
            + DB::albumMap[DB::Album::Artist] + ", "
            + DB::albumMap[DB::Album::NumberOfTracks] + ", "
            + DB::albumMap[DB::Album::DiscNumber] + ", "
            + DB::albumMap[DB::Album::NumberOfDiscs] + ", "
            + DB::albumMap[DB::Album::Year] + ", "
            + DB::albumMap[DB::Album::CoverPath] + ") "
            "VALUES (:name, :sortname, :artist_id, :number_of_tracks, :disc_number, "
            ":number_of_dics, :year, :cover_path)");

        query.addBindValue(album.name());
        query.addBindValue(album.sortName());
        if (!album.artist().name().isEmpty()) {
                Artist artist = album.artist();
                this->insertArtist(artist);
                album.setArtist(artist);
                query.addBindValue(album.artist().id());
            } else {
                query.addBindValue(QVariant(QVariant::Int));
            }
        query.addBindValue(album.numberOfTracks());
        query.addBindValue(album.discNumber());
        query.addBindValue(album.numberOfDiscs());
        query.addBindValue(album.year());
        query.addBindValue(album.coverUrl().toLocalFile());

        if(!query.exec()) {
            DB::error(_ME_,query);
        } else {
            qDebug() << "last " <<query.lastInsertId().toInt();
            album.setId(query.lastInsertId().toInt());
        }
    }
    QList<Genre> genreList = album.genreList();
    this->insertGenres(genreList, album);
    album.setGenreList(genreList);
}

void DatabaseManager::insertGenres(QList<Genre>& genreList, Album& album)
{
    QSqlQuery query(d->db());

    query.prepare("INSERT INTO " + DB::Tables::genres + "("
        + DB::genreMap[DB::Genre::Name] + ") "
        "VALUES (:name)");

    for (Genre& genre : genreList) {
        if (int id = this->existGenre(genre)) {
            genre.setId(id);
        } else {
            query.addBindValue(genre.name());

            if(!query.exec()) {
                DB::error(_ME_, query);
            } else {
                genre.setId(query.lastInsertId().toInt());
            }
        }
    }

    this->linkGenresToAlbum(genreList, album);
}

void DatabaseManager::insertOtherTags(const QList<QPair<QString, QString> >& otherTags,
                                      const Track& track)
{
    QSqlQuery query(d->db());

    query.prepare("INSERT INTO " + DB::Tables::otherTags + "("
        + DB::otherTagMap[DB::OtherTag::Name] + ", "
        + DB::otherTagMap[DB::OtherTag::Value] + ", "
        + DB::otherTagMap[DB::OtherTag::Track] + ") "
        "VALUES (:name, :value, :track)");

    for (auto& otherTag : otherTags) {
        query.addBindValue(otherTag.first);
        query.addBindValue(otherTag.second);
        query.addBindValue(track.id());

        if(!query.exec()) {
            DB::error(_ME_, query);
        }
    }
}

void DatabaseManager::linkGenresToAlbum(QList<Genre>& genreList, Album& album)
{
    QSqlQuery query(d->db());

    query.prepare("INSERT INTO " + DB::Tables::albumsGenres + "("
        + DB::albumGenreMap[DB::AlbumGenre::Album] + ", "
        + DB::albumGenreMap[DB::AlbumGenre::Genre] + ") "
        "VALUES (:album, :genre)");

    for (Genre& genre : genreList) {
        qDebug() << genre.id();
        qDebug() << album.id();
        query.addBindValue(album.id());
        query.addBindValue(genre.id());

        if(!query.exec()) {
            DB::error(_ME_, query);
        }
    }
}
