#include "databasemanager_test.h"
#include "private/databasemanager_p.h"
void DatabaseManagerTest::initTestCase()
{
    QStandardPaths::setTestModeEnabled(true);
    m_dbName = "library";
}

void DatabaseManagerTest::cleanupTestCase()
{
    QString dbPath = QStandardPaths::writableLocation(QStandardPaths::AppDataLocation);
    QDir dir(dbPath);
    QVERIFY(dir.removeRecursively());
}

void DatabaseManagerTest::openDb()
{
    QString dbPath = QStandardPaths::writableLocation(QStandardPaths::AppDataLocation);

    DatabaseManager dbManager(m_dbName, dbPath);
    QVERIFY(dbManager.d->db().isOpen());
    QVERIFY(dbManager.d->db().contains(m_dbName));
    QString databasePath_exp = dbPath + "/" + m_dbName + ".sqlite";

    QCOMPARE(dbManager.d->db().databaseName(), databasePath_exp);
}

QTEST_MAIN( DatabaseManagerTest )
