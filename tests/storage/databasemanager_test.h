#ifndef DATABASEMANAGER_TEST_H
#define DATABASEMANAGER_TEST_H

#include <QtTest>

#include "databasemanager.h"

class DatabaseManagerTest: public QObject
{
    Q_OBJECT
private slots:
    void initTestCase();
    void cleanupTestCase();

    // Here should be for every function a function()
    void openDb();
private:
    QString m_dbName;
};

#endif
